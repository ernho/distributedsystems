#!/bin/bash
#start server

java -cp ../lib/junit.jar:../lib/aws-java-sdk-1.2.1.jar:../lib/AWSBankServer.jar:../lib/commons-codec-1.3.jar:../lib/commons-logging-1.1.1.jar:../lib/httpclient-4.1.1.jar:../lib/httpcore-4.1.jar:../lib/jackson-core-asl-1.4.3.jar:../lib/org.hamcrest.core_1.1.0.v20090501071000.jar:../lib/stax-1.2.0.jar:../lib/stax-api-1.0.1.jar ch.unibas.cs.dbis.dis.aws.server.BankServer $1 $2
