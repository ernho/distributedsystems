package ch.unibas.cs.dbis.dis.aws.db;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;

import ch.unibas.cs.dbis.dis.aws.exception.TransactionExistsException;
import ch.unibas.cs.dbis.dis.aws.exception.UnknownTransactionException;
import ch.unibas.cs.dbis.dis.aws.server.Transaction;



public class PersistentTransactionTable extends TransactionTable {
	private Integer idCounter = new Integer(0);
	
	private AmazonS3 s3; 
	Bucket bucket; 
	
	public PersistentTransactionTable(String bic,AWSCredentials awsCredentials) {
		super(bic);
		this.s3 = new AmazonS3Client(awsCredentials);
		this.bucket = s3.createBucket(bic.toLowerCase()+"transactiontable");
		
			
		
	}
	



	@Override
	public void put(String id, Transaction tx) throws TransactionExistsException, IOException {
			
			if (this.containsId(id)) {
				throw new TransactionExistsException(id, tx);
			}else{
				//warnung 
				InputStream data = serialize(tx);
				s3.putObject(bucket.getName(), id,data , new ObjectMetadata());
			}
			
			
			
	}

	@Override
	public synchronized Transaction get(String id) throws UnknownTransactionException {
		Transaction tr = null;
		try {
		    S3Object obj = s3.getObject(this.bucket.getName(), id);
		     tr= (Transaction) deserialize(obj.getObjectContent());
		  
		   
		} catch (AmazonServiceException e) {
		    	  throw new UnknownTransactionException(id);
		   
		
		} catch (ClassNotFoundException e) {
		
			e.printStackTrace();
			
			// throw new UnknownTransactionException(id);
		} catch (IOException e) {
	
			e.printStackTrace();
			// throw new UnknownTransactionException(id);
		}
		  return tr;
		
	}

	@Override
	public synchronized Set<Entry<String, Transaction>> list() {
		
		ObjectListing s3Objects = s3.listObjects(this.bucket.getName()); 
		List<S3ObjectSummary> list = s3Objects.getObjectSummaries();
	    Hashtable<String, Transaction> table = new Hashtable<String, Transaction>();

		for(S3ObjectSummary object: list) {
		    S3Object obj = s3.getObject(this.bucket.getName(), object.getKey());
		    try {
				table.put(obj.getKey(), (Transaction) deserialize(obj.getObjectContent()));
			} catch (ClassNotFoundException e) {
		
				e.printStackTrace();
			} catch (IOException e) {
			
				e.printStackTrace();
			}
		   
		}
		
		 return new HashSet<Entry<String, Transaction>>(table.entrySet());
	}

	@Override
	public synchronized boolean containsId(String id) {
		 try {
		        s3.getObjectMetadata(bucket.getName(), id); 
		    } catch(AmazonServiceException e) {
		        return false;
		    }
		    return true;
	}

	@Override
	public synchronized void remove(String id) throws UnknownTransactionException {
		try {
			s3.deleteObject(this.bucket.getName(), id);
		} catch (AmazonServiceException e) {
		    String errorCode = e.getErrorCode();
		    if (!errorCode.equals("NoSuchKey")) {
		       throw new UnknownTransactionException(id);
		    }
		
		}
		
		
	}

	@Override
	protected void putIdCounter(int idCounter) {
		synchronized (this.idCounter) {
			this.idCounter = new Integer(idCounter);
		}
	}

	@Override
	protected int getIdCounter() {
		synchronized (idCounter) {
			return this.idCounter.intValue();
		}		
	}
	
    public InputStream serialize(Object obj) throws IOException {
  
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(obj);
        oos.flush();
        oos.close();
        InputStream is = new ByteArrayInputStream(baos.toByteArray());

        return is;
}

public Object deserialize(InputStream s3ObjectInputStream) throws IOException, ClassNotFoundException {
       // ByteArrayInputStream in = new ByteArrayInputStream(s3ObjectInputStream);
        ObjectInputStream ois = new ObjectInputStream(s3ObjectInputStream);
        return ois.readObject();
}
}
