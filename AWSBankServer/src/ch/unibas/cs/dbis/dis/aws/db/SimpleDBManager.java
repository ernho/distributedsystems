package ch.unibas.cs.dbis.dis.aws.db;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ch.unibas.cs.dbis.dis.aws.exception.AccountOverdrawException;
import ch.unibas.cs.dbis.dis.aws.exception.KnownAccountException;
import ch.unibas.cs.dbis.dis.aws.exception.UnknownAccountException;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpledb.AmazonSimpleDB;
import com.amazonaws.services.simpledb.AmazonSimpleDBClient;
import com.amazonaws.services.simpledb.model.BatchPutAttributesRequest;
import com.amazonaws.services.simpledb.model.CreateDomainRequest;
import com.amazonaws.services.simpledb.model.DeleteAttributesRequest;
import com.amazonaws.services.simpledb.model.GetAttributesRequest;
import com.amazonaws.services.simpledb.model.GetAttributesResult;
import com.amazonaws.services.simpledb.model.Item;
import com.amazonaws.services.simpledb.model.ReplaceableAttribute;
import com.amazonaws.services.simpledb.model.ReplaceableItem;
import com.amazonaws.services.simpledb.model.SelectRequest;
import com.amazonaws.services.simpledb.model.SelectResult;

public class SimpleDBManager implements IDatabase {

	private AmazonSimpleDB sdb;
	private String domain = "";
	private String bic;

	public SimpleDBManager(String bic, AWSCredentials awsCredentials) {
		
		this.domain = bic;
		this.bic = bic;
		try {
			this.sdb = new AmazonSimpleDBClient(awsCredentials);
			Region usEast = Region.getRegion(Regions.US_EAST_1);
			this.sdb.setRegion(usEast);
			sdb.createDomain(new CreateDomainRequest(bic));
			System.out.println("DBUG: Domain " + this.domain + " created");
		} catch (AmazonServiceException ase) {
			ase.printStackTrace();
		} catch (AmazonClientException ace) {
			ace.printStackTrace();
		}

	}

	@Override
	public synchronized double getBalance(String iban) throws UnknownAccountException {

		double balance = 0.0;

		GetAttributesResult results = sdb
				.getAttributes(new GetAttributesRequest()
						.withConsistentRead(true).withDomainName(this.domain)
						.withItemName(iban));

		if (results.getAttributes().isEmpty()) {
			throw new UnknownAccountException(this.bic, iban);
		} else {
			balance = Double.parseDouble(results.getAttributes().get(0)
					.getValue());

		}

		return balance;

	}

	@Override
	public synchronized void deposit(String iban, double amount)
			throws UnknownAccountException, IllegalArgumentException {
		if (amount <= 0)
			throw new IllegalArgumentException("Amount is negativ");

		if (checkAccountExist(iban)) {
			double oldBalance = this.getBalance(iban); // throws
													// UnknownAccountException
			double newBalance = oldBalance + amount;

			List<ReplaceableItem> data = new ArrayList<ReplaceableItem>();
			data.add(new ReplaceableItem(iban).withAttributes(new ReplaceableAttribute("balance", ""+ newBalance, true)));
			sdb.batchPutAttributes(new BatchPutAttributesRequest(this.domain,data));

		} else {
			throw new UnknownAccountException(this.bic, iban);

		}

	}

	@Override
	public synchronized void withdraw(String iban, double amount)
			throws UnknownAccountException, AccountOverdrawException,
			IllegalArgumentException, NullPointerException {
		
		if (amount <= 0)
			throw new IllegalArgumentException("Amount is negativ");

		double oldBalance = getBalance(iban);
		double newBalance = oldBalance - amount;

		if (newBalance < 0.0)
			throw new AccountOverdrawException(iban);

		if (checkAccountExist(iban)) {
			List<ReplaceableItem> data = new ArrayList<ReplaceableItem>();
			data.add(new ReplaceableItem(iban)
					.withAttributes(new ReplaceableAttribute("balance", ""
							+ newBalance, true)));
			sdb.batchPutAttributes(new BatchPutAttributesRequest(this.domain,
					data));

		} else {
			throw new UnknownAccountException(this.bic, iban);

		}

	}

	/**
	 * 
	 * Iban is the Itemname (primarykey)
	 * 
	 * 
	 */

	@Override
	public synchronized void addAccount(String iban, double balance)
			throws KnownAccountException, IllegalArgumentException,
			NullPointerException {

		if (iban == null) {
			throw new NullPointerException();

		} else if (iban.equalsIgnoreCase("")) {
			throw new IllegalArgumentException();

		}

		if (checkAccountExist(iban)) {
			throw new KnownAccountException(iban);
		} else {
			List<ReplaceableItem> data = new ArrayList<ReplaceableItem>();
			data.add(new ReplaceableItem(iban)
					.withAttributes(new ReplaceableAttribute("balance", ""
							+ balance, false)));
			sdb.batchPutAttributes(new BatchPutAttributesRequest(this.domain,
					data));
			
			//wait some time because of lazy update
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

		}

	}

	@Override
	public synchronized void  deleteAccount(String iban) throws UnknownAccountException {
		if (checkAccountExist(iban)) {
			sdb.deleteAttributes(new DeleteAttributesRequest(domain, iban));
		} else {
			throw new UnknownAccountException(this.bic, iban);
		}

	}

	@Override
	public synchronized Set<String> listAccounts() {

		Set<String> res = new HashSet<String>();

		String qry = "select * from `" + domain + "`";
		SelectRequest selectRequest = new SelectRequest(qry);
		for (Item item : sdb.select(selectRequest).getItems()) {
			res.add(item.getName());
		}
		return res;

	}

	private synchronized boolean checkAccountExist(String iban) {

		SelectRequest selectRequest = new SelectRequest("select * from `"
				+ this.domain + "` where itemName()='" + iban + "'");
		SelectResult selectResult = sdb.select(selectRequest);
		List<Item> itemList = selectResult.getItems();
		if (itemList.isEmpty()) {
			return false;
		} else {
			return true;
		}

	}

}
