import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.XAConnection;
import javax.transaction.xa.XAException;
import javax.transaction.xa.XAResource;
import javax.transaction.xa.Xid;

import oracle.jdbc.xa.OracleXid;
import oracle.jdbc.xa.client.OracleXADataSource;


public class XATest {
	
		static float value = 100.5f;
		static String fromIBAN = "CH5367A1";
		static String fromBIC = "p4";
		static String toIBAN = "CH5367A1";
        static String toBIC = "p5";
	  /**
	  *  Creates a unique Transaction Id for each branch
	  *  Transaction Id will be identical for all the branches under the
	  *  same distributed transaction. 
	  *  Branch Id is unique for each branch under the same
	  *  distributed transaction.
	  **/
	  static Xid createXid(int transId, int branchId) throws XAException {
	      
		//The identifiers are 64 bytes each  
	    byte[] globalTransId    = new byte[64];
	    byte[] branchQualifier = new byte[64];

	    //Here we use only 1 byte for the exercise
	    globalTransId[0]=(byte)transId;
	    branchQualifier[0]=(byte)branchId;

	    // Create the Oracle Transaction Id
	    // Transaction Id has 3 components
	    Xid xid = new OracleXid(0x1234, // Format identifier
	                globalTransId, // Global transaction identifier
	                branchQualifier); // Branch qualifier

	    return xid;
	  }
	  
	/**
	 * The main method
	 */
	public static void main(String[] args) {
	  
		  //The Oracle XA data source objects
		  OracleXADataSource oxa1 = null;
		  OracleXADataSource oxa2 = null;
		  
		  //The XA Ressource objects
		  XAResource xares1 = null;
		  XAResource xares2 = null;
		  
		  //The XA connections
		  XAConnection xacon1 =null;
		  XAConnection xacon2 =null;
		  
		  //The transaction identifiers
	      Xid xid1=null;
	      Xid xid2=null;
	      
	      //The JDBC connections
	      Connection c1=null;
	      Connection c2=null;
	      
	      //Some Flags to indicate if the execution of the transaction branch was successful
	      boolean branch1Ok = true;
	      boolean branch2Ok = true;
	      
	      //Variables for storing the result of prepare to commit
	      int prepareResultBranch1=-1;
	      int prepareResultBranch2=-1;
	      
	      //Initialisation of the first transaction branch
	      try { 	  
	    	//Connect to the database
	      	oxa1 = new OracleXADataSource();  
	      	oxa1.setURL("jdbc:oracle:thin:@"+fromBIC+".dbis.cs.unibas.ch:1521:orcl"+fromBIC);
		    oxa1.setUser("cs341_5");
		    oxa1.setPassword("pURVrBPm");
		      
	        //get a XA connection
	        xacon1 = oxa1.getXAConnection();
	        
	        //get a normal JDBC connection 	
	        c1 = xacon1.getConnection();
	        //Do not use autocommit for SQL operations
	        c1.setAutoCommit(false);
	        
	        if (c1==null) {
	        	System.err.println("no connection");
	        	return;
	        }
	        
	        //Create a XAResource object for the given XA connection
	        xares1=xacon1.getXAResource();
	        
	        //Look for pending transaction branches
	        Xid xids[] = xares1.recover(XAResource.TMSTARTRSCAN);
	        System.out.println("Found "+xids.length+" pending transaction branches!");
	        
	        //Perform a Rollback of all pending transaction branches
	        for (int i=0;i<xids.length;i++) 
	        {
	          System.out.println("Rollback of transaction branch XID: "+xids[i].getGlobalTransactionId()[0]+":"+xids[i].getBranchQualifier()[0]);
	          xares1.rollback(xids[i]);
	          i++;
	        }
	        
	        //Create a new transaction ID for this branch transaction: 19 branch: 1
	        xid1 = createXid(19,1);
	      
	      } catch (Exception e) 
	      {
	        System.err.println("Exeption during initialisation of first branch! "+e.getMessage());
	        e.printStackTrace();
	        return;
	      }

	      //Start of the first XA Branch
	      try {
	        xares1.start(xid1,XAResource.TMNOFLAGS);
	      } catch (Exception e) 
	      {
	        System.out.println("Error during start of the first branch! "+e.getMessage());
	        e.printStackTrace();
	        return;
	      }
	      
	      //Here the actual work of the transaction branch is performed
	      //Attention you have to remember if SQL statements in a branch were successful or not 
	      try { 
	    	  PreparedStatement ps1 = c1.prepareStatement("UPDATE account SET balance = balance - ? WHERE IBAN=?");
	    	  ps1.setFloat(1, value);
	    	  ps1.setString(2, fromIBAN);
	    	  
	    	  ps1.executeUpdate(); 
	      }
	        catch (SQLException e) 
	      {
	        //Error in SQL statement
	        branch1Ok=false;
	        System.out.println("Error in SQL statement of first branch! "+e.getMessage());
	      }
	          
	      //End of the first transaction branch
	      try {
	        System.out.println("The first XA branch has finished execution with result: "+branch1Ok);
	        if (branch1Ok) 
	          xares1.end(xid1,XAResource.TMSUCCESS);
	        else
	          xares1.end(xid1,XAResource.TMFAIL);
	      } catch (XAException e) 
	      {
	        System.out.println("Exception on ending first XA branch! "+e.errorCode);
	      }  
	      
	      
	      //Other XA branches may be inserted here 
	      //or if you like you can also implement a multithreaded version where branches are executed in parallel 
	      //TODO: insert second branch here 
	      
	    //Initialisation of the SECOND transaction branch
	      try { 	  
	    	//Connect to the database
	      	oxa2 = new OracleXADataSource();  
	      	oxa2.setURL("jdbc:oracle:thin:@"+toBIC+".dbis.cs.unibas.ch:1521:orcl"+toBIC);
		    oxa2.setUser("cs341_5");
		    oxa2.setPassword("pURVrBPm");
		      
		      
	        //get a XA connection
	        xacon2 = oxa2.getXAConnection();
	        
	        //get a normal JDBC connection 	
	        c2 = xacon2.getConnection();
	        //Do not use autocommit for SQL operations
	        c2.setAutoCommit(false);
	        
	        if (c2==null) {
	        	System.err.println("no connection");
	        	return;
	        }
	        
	        //Create a XAResource object for the given XA connection
	        xares2=xacon2.getXAResource();
	        
	        //Look for pending transaction branches
	        Xid xids[] = xares2.recover(XAResource.TMSTARTRSCAN);
	        System.out.println("Found "+xids.length+" pending transaction branches!");
	        
	        //Perform a Rollback of all pending transaction branches
	        for (int i=0;i<xids.length;i++) 
	        {
	          System.out.println("Rollback of transaction branch XID: "+xids[i].getGlobalTransactionId()[0]+":"+xids[i].getBranchQualifier()[0]);
	          xares2.rollback(xids[i]);
	          i++;
	        }
	        
	        //Create a new transaction ID for this branch transaction: 19 branch: 2
	        xid2 = createXid(19,2);
	      
	      } catch (Exception e) 
	      {
	        System.err.println("Exeption during initialisation of second branch! "+e.getMessage());
	        e.printStackTrace();
	        return;
	      }

	      //Start of the second XA Branch
	      try {
	        xares2.start(xid2,XAResource.TMNOFLAGS);
	      } catch (Exception e) 
	      {
	        System.out.println("Error during start of the second branch! "+e.getMessage());
	        e.printStackTrace();
	        return;
	      }
	      
	      //Here the actual work of the transaction branch is performed
	      //Attention you have to remember if SQL statements in a branch were successful or not 
	      try {
	    	  PreparedStatement ps2 = c2.prepareStatement("UPDATE account SET balance = balance + ? WHERE IBAN=?");
	    	  ps2.setFloat(1, value);
	    	  ps2.setString(2, fromIBAN);
	    	  
	    	  ps2.executeUpdate(); 
	      }
	        catch (SQLException e) 
	      {
	        //Error in SQL statement
	        branch2Ok=false;
	        System.out.println("Error in SQL statement of second branch! "+e.getMessage());
	      }
	          
	      //End of the second transaction branch
	      try {
	        System.out.println("The second XA branch has finished execution with result: "+branch2Ok);
	        if (branch2Ok) 
	          xares2.end(xid2,XAResource.TMSUCCESS);
	        else
	          xares2.end(xid2,XAResource.TMFAIL);
	      } catch (XAException e) 
	      {
	        System.out.println("Exception on ending second XA branch! "+e.errorCode);
	      }  
	      
	      
	      
	      
	      //Finally we come the core of 2PhaseCommit protocol 
	      //First the prepare to commit of first branch (only if successful
	      try {
	    	if (branch1Ok) {
	    		System.out.println("Executing prepare to commit of first branch");
	    		prepareResultBranch1=xares1.prepare(xid1);
	    	} else {
	    		System.out.println("No prepare to commit of first branch because branch was not successful");
	    	}
	      } catch (XAException e) 
	      {
	        System.out.println("Fehler in prepare to commit! "+e.errorCode);
	      } 
	      
	      //Other branches Prepare to commit here
	      try {
		    	if (branch2Ok) {
		    		System.out.println("Executing prepare to commit of second branch");
		    		prepareResultBranch2=xares2.prepare(xid2);
		    	} else {
		    		System.out.println("No prepare to commit of second branch because branch was not successful");
		    	}
		      } catch (XAException e) 
		      {
		        System.out.println("Fehler in prepare to commit! "+e.errorCode);
		      } 
	  
	      
	      //Now the final decision if all branches successful go for commit on all branches otherwise rollback all
	      //Attention here only one branch is evaluated!
	      try {
	        System.out.println("Result of prepare to commit of first branch: "+prepareResultBranch1);
	        System.out.println("Result of prepare to commit of second branch: "+prepareResultBranch2);
	        //Prepare OK => Commit
	        if (prepareResultBranch1==XAResource.XA_OK && prepareResultBranch2==XAResource.XA_OK)
	        {
	          System.out.println("Commit of all branches!");
	          xares1.commit(xid1,false); 
	          xares2.commit(xid2,false); 
	          //Read only transactions do not need commit or rollback
	        } 
	        else if (prepareResultBranch1!=XAResource.XA_RDONLY && prepareResultBranch2!=XAResource.XA_RDONLY) 
	        {
	          System.out.println("Rollback of all branches due to failures!");
	          xares1.rollback(xid1);
	        } 
	      }catch (XAException e) 
	      {
	        System.out.println("XA Commit/Rollback not possible! "+e.errorCode);
	      }
	      
	      //Finally we close the connections
	      try {
	    	  c1.close();
	    	  c2.close();
	    	  xacon1.close();
	    	  xacon2.close();
	      } catch (SQLException e) {
	    	  System.err.println("Error on closing connections "+e.getMessage());
	    	  e.printStackTrace();
	      }
	      

	   }

}
