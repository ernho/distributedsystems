package ch.unibas.cs.dbis.dis.aws;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

public class SSHConnection extends Thread {

	private static JSch jsch;
	private static Session session;

	String command = null, address = null;
	static boolean close = false;

	/**
	 * @param args
	 */
	public SSHConnection(String _address, String _command) {
		address = _address;
		command = _command;

		try {
			createSSHConnection(address);
		} catch (Exception e2) {
			System.err.println("could not set up a connection:");
			e2.printStackTrace();
		}
	}

	public void run() {
		List<String> commands = new ArrayList<String>();

		commands.add(command);

		try {
			executeRemoteCommand(commands);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	private static void createSSHConnection(String address) throws Exception {
		jsch = new JSch();
		jsch.addIdentity("/home/ernad/.ec2/temp/ernad-distri.pem");
		session = jsch.getSession("ubuntu", address, 22);
		java.util.Properties config = new java.util.Properties();
		config.put("StrictHostKeyChecking", "no");
		config.put("cipher.s2c", "aes128-cbc,3des-cbc");
		session.setConfig(config);
		session.connect();
		System.out.println("SSH connection created");
	}

	public void executeRemoteCommand(List<String> commands)
			throws Exception {
		
		System.out.println("excute remote comannds");

	
		
		Channel channel = session.openChannel("shell");
		channel.connect();
		
		OutputStream inputstream_for_the_channel = channel.getOutputStream();
		PrintStream commander = new PrintStream(inputstream_for_the_channel,
				true);
		for (int i = 0; i < commands.size(); i++) {
			commander.println(commands.get(i));
		}
		commander.println("exit");
		commander.close();
		
		InputStream in = channel.getInputStream();
		byte[] tmp = new byte[1024];
		while (!close) {
			while (in.available() > 0 && !close) {
				int i = in.read(tmp, 0, 1024);
				if (i < 0)
					break;
				String str = new String(tmp, 0, i);

		
				
				
			}
			try {
				Thread.sleep(1000);
			} catch (Exception ee) {
				ee.printStackTrace();
			}
		}

		
		
		channel.disconnect();
	}
	
	public void close() {
		close = true;
	}

}
