package ch.unibas.cs.dbis.dis.aws;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;



import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.PropertiesCredentials;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.Reservation;
import com.amazonaws.services.ec2.model.RunInstancesRequest;
import com.amazonaws.services.ec2.model.RunInstancesResult;
import com.amazonaws.services.ec2.model.TerminateInstancesRequest;

public class MyAWSEnvironment {

	private static String address;
	private static AmazonEC2 ec2;
	private static Scanner in;

	public static void main(String[] args) throws Exception {
		System.out.print("Setting up the EC2 handler...");
		setupEC2();
		System.out.println("Setting done.");

		System.out.print("Starting up an instance...");
		ArrayList<String> instanceIds = runInstance();
		System.out.println("Starting done.");

		System.out.print("Retrieving the address...");
		address = getAddressOfInstance(instanceIds);
		System.out.println("Retrieving done.\n");

		System.out
				.println("The instance " + instanceIds.get(0) + " is running");
		System.out.println("Instance ip address: " + address);

		runDatabaseServerClient();

		//terminateInstance(instanceIds);
	}

	private static void setupEC2() {
		AWSCredentials credentials = null;

		try {
			credentials = new PropertiesCredentials(
					MyAWSEnvironment.class
							.getResourceAsStream("AwsCredentials.properties"));
		} catch (IOException e1) {
			System.out
					.println("Credentials were not properly entered into AwsCredentials.properties.");
			System.out.println(e1.getMessage());
			System.exit(-1);
		}

		ec2 = new AmazonEC2Client(credentials);
		ec2.setEndpoint("ec2.us-east-1.amazonaws.com");
	}

	private static ArrayList<String> runInstance() {
		// Create an instance of our AMI
		RunInstancesRequest runInstancesRequest = new RunInstancesRequest()
				.withInstanceType("m1.small").withImageId("ami-13cfe37a")
				.withMinCount(1).withMaxCount(1).withKeyName("ernad-distri")
				.withSecurityGroupIds("default");

		// run the instance
		RunInstancesResult runInstances = ec2.runInstances(runInstancesRequest);

		// retrieve the id
		List<Instance> instances = runInstances.getReservation().getInstances();
		ArrayList<String> instanceIds = new ArrayList<String>();
		for (Instance instance : instances)
			instanceIds.add(instance.getInstanceId());
		return instanceIds;
	}

	private static String getAddressOfInstance(ArrayList<String> instanceIds)
			throws InterruptedException {
		boolean running = false;
		String address = null;

		while (!running) {
			Thread.sleep(2000);

			List<Reservation> reservationList = ec2
					.describeInstances(
							new DescribeInstancesRequest()
									.withInstanceIds(instanceIds))
					.getReservations();
			List<Instance> instanceList = reservationList.get(0).getInstances();

			String status = instanceList.get(0).getState().getName();
			running = status.equals("running");

			if (running)
				address = instanceList.get(0).getPublicDnsName();
		}
		return address;
	}

	private static void runDatabaseServerClient() {
		in = new Scanner(System.in);

		if (address == null || address.equals("")) {
			System.err.println("Address not valid");
		} else {
			SSHConnection runderby = execute(
					"\nEnter ok to start the derby database:",
					"java -jar /usr/local/db-derby-10.9.1.0/lib/derbyrun.jar server start");
			SSHConnection runjboss = execute(
					"enter ok to start the jboss server:",
					"/home/ubuntu/jboss-5.1.0_derby/bin/run.sh -c dis -b "
							+ address);

			System.out
					.println("....server is starting");
			in.next();

			runderby.close();
			runjboss.close();
		}
	}

	private static SSHConnection execute(String toShow, String command) {
		System.out.println(toShow);
		in.next();
		SSHConnection conn = new SSHConnection(address, command);
		conn.start();
		return conn;
	}

	private static void terminateInstance(ArrayList<String> instanceIds) {
		try {
			// Terminate instances.
			TerminateInstancesRequest terminateRequest = new TerminateInstancesRequest(
					instanceIds);
			ec2.terminateInstances(terminateRequest);
		} catch (AmazonServiceException e) {
			System.out.println("Error terminating instances");
			System.out.println("Caught Exception: " + e.getMessage());
			System.out.println("Reponse Status Code: " + e.getStatusCode());
			System.out.println("Error Code: " + e.getErrorCode());
			System.out.println("Request ID: " + e.getRequestId());
		}

		System.out.println("\nInstance " + instanceIds.get(0)
				+ " terminated succesfully!");
	}
}