/*
 * Created on 04.09.2008
 *
 * (c) 2008 Uni Basel Switzerland - Thorsten M�ller
 */
package ch.unibas.cs.dbis.dis.ejb3;


/**
 *
 * @author Thorsten M�ller - Thorsten.Moeller@unibas.ch
 * @version $Rev: 107 $; $Author: thorsten $; $Date: 2008-09-04 17:54:06 +0200 (Do, 04 Sep 2008) $
 */
public interface FinanceRemote extends Finance
{

}
