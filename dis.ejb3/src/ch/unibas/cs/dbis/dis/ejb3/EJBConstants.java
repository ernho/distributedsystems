/*
 * Created on 15.11.2006
 *
 * (c) 2006 Uni Basel Switzerland - Thorsten M�ller
 */
package ch.unibas.cs.dbis.dis.ejb3;


/**
 *
 * @author Thorsten M�ller - Thorsten.Moeller@unibas.ch
 * @version $Rev: 110 $; $Author: thorsten $; $Date: 2008-10-17 20:42:35 +0200 (Fr, 17 Okt 2008) $
 */
public interface EJBConstants
{
	public static final String Q_FIND_CUSTOMERS_BY_FULL_NAME = "queryCustomersByFullName";
	public static final String Q_FIND_ALL_BANKS = "findAllBanks";

	public static final String JNDI_FINANCE_LOCAL_NAME = FinanceEJB.class.getSimpleName() + "/local";
	public static final String JNDI_FINANCE_REMOTE_NAME = FinanceEJB.class.getSimpleName() + "/remote";
	public static final String JNDI_EXT_FINANCE_REMOTE_NAME = ExtendedFinanceEJB.class.getSimpleName() + "/remote";
//	public static final String JNDI_GEOMETRIC_LOCAL_NAME = GeometricEJB.class.getSimpleName() + "/local";
//	public static final String JNDI_GEOMETRIC_REMOTE_NAME = GeometricEJB.class.getSimpleName() + "/remote";
}
