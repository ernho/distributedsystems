/*
 * Created on 17.10.2008
 *
 * (c) 2008 Thorsten M�ller - University of Basel Switzerland
 */
package ch.unibas.cs.dbis.dis.ejb3;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicLong;

/**
 *
 * @author Thorsten M�ller - Thorsten.Moeller@unibas.ch
 * @version $Rev: 150 $; $Author: thorsten $; $Date: 2010-10-18 16:53:09 +0200 (Mo, 18 Okt 2010) $
 */
public class ExtendedFinanceEJB extends FinanceEJB implements ExtendedFinance
{
	/** Used for thread safe creation of 'unique' block transfer IDs. */
	private static final AtomicLong blockTransferSequence = new AtomicLong(1);

	/** Stores multiple block transfers unless they will be performed or canceled. */
	private final Map<Long, BlockTransfer> blockTransfers;

	public ExtendedFinanceEJB()
	{
		blockTransfers = new HashMap<Long, BlockTransfer>();
	}

	/* @see ch.unibas.cs.dbis.dis.ejb3.ExtendedFinance#addToBlockTransfer(ch.unibas.cs.dbis.dis.ejb3.Account, double, long) */
	public void addToBlockTransfer(final Account target, final double amount, final long blockTransferID)
		throws IllegalArgumentException
	{
		final BlockTransfer bts = blockTransfers.get(Long.valueOf(blockTransferID));
		if (bts == null) throw new IllegalArgumentException("Block transfer ID " + blockTransferID + " unknown.");
		bts.addTransfer(target, amount);
	}

	/* @see ch.unibas.cs.dbis.dis.ejb3.ExtendedFinance#cancelBlockTransfer(long) */
	public void cancelBlockTransfer(final long blockTransferID) throws IllegalArgumentException
	{
		final BlockTransfer bts = blockTransfers.remove(Long.valueOf(blockTransferID));
		if (bts == null) throw new IllegalArgumentException("Block transfer ID " + blockTransferID + " unknown.");
		// GC does the rest
	}

	/* @see ch.unibas.cs.dbis.dis.ejb3.ExtendedFinance#createBlockTransfer(ch.unibas.cs.dbis.dis.ejb3.Account) */
	public long createBlockTransfer(final Account source)
	{
		final long nextID = blockTransferSequence.incrementAndGet();
		blockTransfers.put(Long.valueOf(nextID), new BlockTransfer(source));
		return nextID;
	}

	/* @see ch.unibas.cs.dbis.dis.ejb3.ExtendedFinance#performBlockTransfer(long) */
	public void performBlockTransfer(final long blockTransferID) throws AccountOverdrawException,
		IllegalArgumentException
	{
	 	// TODO implement
	}

	/** Helper class. */
	private static final class BlockTransfer implements Iterable<Entry<Account, Double>>
	{
		private final Account source;
		private final Map<Account, Double> transfers;

		BlockTransfer(final Account source)
		{
			this.source = source;
			transfers = new HashMap<Account, Double>();
		}

		void addTransfer(final Account target, final double amount)
		{
			transfers.put(target, Double.valueOf(amount));
		}

		Account getSourceAccount()
		{
			return source;
		}

		/* @see java.lang.Iterable#iterator() */
		public Iterator<Entry<Account, Double>> iterator()
		{
			return transfers.entrySet().iterator();
		}
	}
}
