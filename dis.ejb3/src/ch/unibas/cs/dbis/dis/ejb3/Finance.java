/*
 * Created on 15.11.2006
 *
 * (c) 2006 Uni Basel Switzerland - Thorsten M�ller
 */
package ch.unibas.cs.dbis.dis.ejb3;

import java.util.Collection;

/**
 *
 * @author Thorsten M�ller - Thorsten.Moeller@unibas.ch
 * @version $Rev: 110 $; $Author: thorsten $; $Date: 2008-10-17 20:42:35 +0200 (Fr, 17 Okt 2008) $
 */
public interface Finance
{
	/**
	 * Transfers the given value from the source account to the target account.
	 *
	 * @param from The source account.
	 * @param to The target account.
	 * @param value The value to transfer.
	 * @throws AccountOverdrawException In case the source account balance is
	 * 	less than the given transfer value.
	 * @throws IllegalArgumentException If the given transfer value is less
	 * 	than <tt>0</tt>.
	 */
	public void transfer(Account from, Account to, double value) throws AccountOverdrawException, IllegalArgumentException;

	/**
	 * Retrieves some particular account.
	 *
	 * @param accountID The ID of the account to retrieve.
	 * @return The account of the given ID or <code>null</code> if an account
	 * 	with this ID does not exist.
	 */
	public Account findAccount(long accountID);

	/**
	 * Retrieves some particular customer.
	 *
	 * @param customerID The ID of the customer to retrieve.
	 * @return The customer of the given ID or <code>null</code> if an customer
	 * 	with this ID does not exist.
	 */
	public Customer findCustomer(long customerID);

	/**
	 * Retrieves all banks known in the system.
	 *
	 * @return A collection of all banks that exist.
	 */
	public Collection<Bank> findBanks();

	/**
	 * Simple search method to retrieve customers according to their name.
	 *
	 * @param firstname The first name of the person. <code>null</code> serves
	 * 	as the wildcard.
	 * @param surname The last name of the person. <code>null</code> servers
	 * 	as the wildcard.
	 * @return The customer(s) of the given names.
	 */
	public Collection<Customer> queryCustomers(String firstname, String surname);

	/**
	 * Add a new bank to the system.
	 *
	 * @param b The new bank to insert.
	 * @return The ID that was assigned to the bank.
	 * @throws InsertException
	 */
	public long insertBank(Bank b) throws InsertException;

	/**
	 * Add a new customer to the system.
	 *
	 * @param c The new customer to insert. Note that it must refer to some
	 * 	existing bank and at least one new account. The latter will be
	 * 	inserted as well.
	 * @return The ID that was assigned to the customer.
	 * @throws InsertException
	 */
	public long insertCustomer(Customer c) throws InsertException;
}