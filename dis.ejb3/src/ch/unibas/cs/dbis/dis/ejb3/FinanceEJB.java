/*
 * Created on 15.11.2006
 *
 * (c) 2006 Uni Basel Switzerland - Thorsten M�ller
 */
package ch.unibas.cs.dbis.dis.ejb3;

import java.util.Collection;

import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.FlushModeType;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Thorsten M�ller - Thorsten.Moeller@unibas.ch
 * @version $Rev: 110 $; $Author: thorsten $; $Date: 2008-10-17 20:42:35 +0200 (Fr, 17 Okt 2008) $
 */
@Stateless(name = "FinanceEJB")
@Remote(FinanceRemote.class) 
@Local(FinanceLocal.class)
@TransactionManagement(TransactionManagementType.CONTAINER)
public class FinanceEJB implements Finance
{
	@PersistenceContext
	protected EntityManager em;

	@Resource
	protected EJBContext context;

	/* @see ch.unibas.cs.dbis.dis.ejb3.Finance#transfer(ch.unibas.cs.dbis.dis.ejb3.Account, ch.unibas.cs.dbis.dis.ejb3.Account, double) */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void transfer(final Account from, final Account to, final double value) throws AccountOverdrawException, IllegalArgumentException
	{
		try{
		from.withdraw(value);
		to.deposit(value);
		}
		catch(AccountOverdrawException e){
			context.setRollbackOnly();
			throw e;
		}
		catch(IllegalArgumentException e){
			context.setRollbackOnly();
			throw e;	
		}
		em.merge(from);
		em.merge(to);
		em.flush(); //do we need this?
	}

	/* @see ch.unibas.cs.dbis.dis.ejb3.Finance#findAccount(long) */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)

	public Account findAccount(final long accountID)
	{
		return em.find(Account.class, accountID);
	}

	/* @see ch.unibas.cs.dbis.dis.ejb3.Finance#findCustomer(long) */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Customer findCustomer(final long customerID)
	{
		return em.find(Customer.class, customerID);
	}

	/* @see ch.unibas.cs.dbis.dis.ejb3.Finance#queryCustomers(java.lang.String, java.lang.String) */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Collection<Customer> queryCustomers(final String firstname, final String surname)
	{
		final Query q = em.createNamedQuery(EJBConstants.Q_FIND_CUSTOMERS_BY_FULL_NAME);
		q.setFlushMode(FlushModeType.AUTO);
		q.setParameter(1, firstname);
		q.setParameter(2, surname);
		return q.getResultList();
	}

	/* @see ch.unibas.cs.dbis.dis.ejb3.Finance#findBanks() */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Collection<Bank> findBanks()
	{
		final Query q = em.createNamedQuery(EJBConstants.Q_FIND_ALL_BANKS);
		return q.getResultList();
	}

	/* @see ch.unibas.cs.dbis.dis.ejb3.Finance#insertBank(ch.unibas.cs.dbis.dis.ejb3.Bank) */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public long insertBank(final Bank b)
	{
		if (b != null) em.persist(b);
		return b.getBankID();
	}

	/* @see ch.unibas.cs.dbis.dis.ejb3.Finance#insertCustomer(ch.unibas.cs.dbis.dis.ejb3.Customer) */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public long insertCustomer(final Customer c)
	{
		if (c != null) em.persist(c);
		return c.getCustomerID();

	}

}
