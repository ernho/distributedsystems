/*
 * Created on 15.11.2006
 *
 * (c) 2006-2008 Thorsten M�ller - University of Basel Switzerland
 */
package ch.unibas.cs.dbis.dis.ejb3;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;
import java.util.Random;

import javax.naming.Context;
import javax.naming.InitialContext;

import junit.framework.TestCase;

/**
 *
 * @author Thorsten M�ller - Thorsten.Moeller@unibas.ch
 * @version $Rev: 114 $; $Author: thorsten $; $Date: 2008-10-27 10:39:12 +0100 (Mo, 27 Okt 2008) $
 */
public class FinanceTest extends TestCase
{
	// must be static since JUnit runs each test method with own instance of this class
	private static boolean nevv = true;
	private static Context context;
	private static Finance finance;
	private static ExtendedFinance extFinance;
	private static final Random random = new Random();
	private static final List<Bank> banks = new ArrayList<Bank>();
	private static final String BANK_ZUERICH = "SuperDuper Z�rich Bank";
	private static final String BANK_BASEL = "SuperDuper Basel Bank";
	private static final String CUSTOMER_FIRSTNAME = "Reto";
	private static final String CUSTOMER_SURNAME = "R�tli";
	private static final int NUMBER_OF_CUSTOMERS = 1000;
	private static final int MAX_ACCOUNT_BALANCE = 50000;


	/* @see junit.framework.TestCase#setUp() */
	@Override protected void setUp() throws Exception
	{
		super.setUp();

		if (nevv)
		{
			// get reference to finance session EJB
			final Properties props = new Properties();
			props.setProperty(Context.INITIAL_CONTEXT_FACTORY, "org.jnp.interfaces.NamingContextFactory");
			props.setProperty(Context.URL_PKG_PREFIXES, "org.jboss.naming.client");
			props.setProperty(Context.PROVIDER_URL, "jnp://localhost:1099");
			context = new InitialContext(props);
			finance = (Finance) context.lookup(EJBConstants.JNDI_FINANCE_REMOTE_NAME);
			//extFinance = (ExtendedFinance) context.lookup(EJBConstants.JNDI_EXT_FINANCE_REMOTE_NAME);

			// prepare for tests by creating some test data
			setUpBanks();
			setUpCustomersAndAccounts();

			nevv = false;
		}
	}

	/**
	 * Add {@link #NUMBER_OF_CUSTOMERS} customers uniformly distributed
	 * among the number of banks which were created in {@link #setUpBanks()}
	 * and add one account per customer.
	 */
	private void setUpCustomersAndAccounts() throws Exception
	{
		Customer c;
		Account a;
		int bankIndex;
		final long time = System.currentTimeMillis();
		for (int i = 0; i < NUMBER_OF_CUSTOMERS; i++)
		{
			c = new Customer(CUSTOMER_FIRSTNAME + i, CUSTOMER_SURNAME + i);
			a = new Account(c);
			a.setBalance(random.nextInt(MAX_ACCOUNT_BALANCE));
			c.addAccount(a);

			bankIndex = i % banks.size();
			c.setBank(banks.get(bankIndex));
			a.setBank(banks.get(bankIndex));

			finance.insertCustomer(c);
		}
		System.out.println("Insertion of " + NUMBER_OF_CUSTOMERS + " customers took "
			+ (System.currentTimeMillis() - time) + "ms.");
	}

	/**
	 *
	 */
	private void setUpBanks() throws Exception
	{
		final long time = System.currentTimeMillis();
		Bank b = new Bank(BANK_BASEL);
		b.setBankID(finance.insertBank(b));
		banks.add(b);
		b = new Bank(BANK_ZUERICH);
		b.setBankID(finance.insertBank(b));
		banks.add(b);
		System.out.println("Insertion of " + banks.size() + " banks took "
			+ (System.currentTimeMillis() - time) + "ms.");
	}

	/* @see junit.framework.TestCase#tearDown() */
	@Override protected void tearDown() throws Exception
	{
		super.tearDown();
	}

	/**
	 * Transfer funds between randomly selected accounts until one account would
	 * be overdrawn. Test that in that situation the balance of the involved
	 * accounts remains the same as before, i.e., the transaction was rolled back.
	 */
	public void testTransfer()
	{
		int count = 0;
		boolean overdraw = false;
		Customer payer, payee;
		Account payerAccount, payeeAccount;
		double payerBalance, payeeBalance, transferFund;
		while (!overdraw)
		{
			do
			{
				payer = finance.findCustomer(random.nextInt(NUMBER_OF_CUSTOMERS));
				payee = finance.findCustomer(random.nextInt(NUMBER_OF_CUSTOMERS));
			}
			while (payer == null || payee == null || payer.getCustomerID() == payee.getCustomerID());
			// OK, now we have two randomly selected and distinct customers (but we do
			// not care whether their accounts are at distinct banks!)

			payerAccount = payer.getAccounts().get(0);
			payeeAccount = payee.getAccounts().get(0);

			payerBalance = payerAccount.getBalance();
			payeeBalance = payeeAccount.getBalance();

			transferFund = random.nextInt(MAX_ACCOUNT_BALANCE / 20); // delay occurrence of overdraw event

			try
			{
				finance.transfer(payerAccount, payeeAccount, transferFund);

				// Get objects again from server to get state after transfer
				payerAccount = finance.findAccount(payerAccount.getAccountID());
				payeeAccount = finance.findAccount(payeeAccount.getAccountID());

				// Verify that balances have changed, i.e., transaction was committed
				assertEquals(payerBalance - transferFund, payerAccount.getBalance(), 0.00001);
				assertEquals(payeeBalance + transferFund, payeeAccount.getBalance(), 0.00001);
			}
			catch (final AccountOverdrawException e)
			{
				overdraw = true;
				System.out.println(System.getProperty("line.separator") + "Iteration " + count
					+ ": transfer from " + payerAccount + " to " + payeeAccount + " not possible with fund "
					+ transferFund);

				// Get objects again from server to get state after exception
				payerAccount = finance.findAccount(payerAccount.getAccountID());
				payeeAccount = finance.findAccount(payeeAccount.getAccountID());

				// Verify that balances are equal, i.e., transaction was rolled back
				assertEquals(payerBalance, payerAccount.getBalance(), 0.00001);
				assertEquals(payeeBalance, payeeAccount.getBalance(), 0.00001);
			}
			count++;
			if (count % 10 == 0) System.out.print(".");
		}
	}

	/**
	 * Query for randomly selected customer and test that at least one customer exists
	 * (more than one customer if test was executed more than once) and that names of
	 * found customers correspond to query criteria.
	 */
	public void testQueryCustomers()
	{
		final int r = random.nextInt(NUMBER_OF_CUSTOMERS);
		final Collection<Customer> c = finance.queryCustomers(CUSTOMER_FIRSTNAME + r, CUSTOMER_SURNAME + r);
		assertNotNull(c);
		assertTrue("At least one customer should have been found.", c.size() >= 1);
		for (final Customer customer : c)
		{
			assertEquals(CUSTOMER_FIRSTNAME + r, customer.getFirstname());
			assertEquals(CUSTOMER_SURNAME + r, customer.getSurname());
		}
	}

	/**
	 * Find randomly selected account, ensure that it was found, and test that it
	 * corresponds to the search criteria. Furthermore, ensure that the account
	 * has both a bank and a customer associated.
	 */
	public void testFindAccount()
	{
		// first, get customer(s) since we don't know the account IDs assigned during insertion
		final long cID = random.nextInt(NUMBER_OF_CUSTOMERS);
		long aID;
		final Collection<Customer> cs = finance.queryCustomers(CUSTOMER_FIRSTNAME + cID, CUSTOMER_SURNAME + cID);
		assertNotNull(cs);

		for (final Customer c : cs)
		{
			//	get the account ID of that customer and find that account explicetly
			aID = c.getAccounts().get(0).getAccountID();
			final Account a = finance.findAccount(aID);
			assertNotNull(a);
			assertEquals(aID, a.getAccountID());
			assertNotNull(a.getBank());
			assertNotNull(a.getCustomer());
		}
	}

	/**
	 * Find randomly selected customer, ensure that it was found, and test that it
	 * corresponds to the search criteria. Furthermore, ensure that the customer
	 * has associated a bank and at least one account (this works only if it is
	 * ensured that the associated objects are loaded eagerly by the container!).
	 */
	public void testFindCustomer()
	{
		long cID = random.nextInt(NUMBER_OF_CUSTOMERS);

		// first, get customer(s) by name since we don't know the customer IDs assigned during insertion
		final Collection<Customer> cs = finance.queryCustomers(CUSTOMER_FIRSTNAME + cID, CUSTOMER_SURNAME + cID);
		assertNotNull(cs);

		for (Customer c : cs)
		{
			cID = c.getCustomerID();
			// reassign the customer to test finder method
			c = finance.findCustomer(c.getCustomerID());
			assertNotNull(c);
			assertEquals(cID, c.getCustomerID());
			assertNotNull(c.getBank());
			assertNotNull(c.getAccounts());
			assertTrue(c.getAccounts().size() > 0);
		}
	}

	/**
	 * Query for all banks. Ensure that not none exist and that the collection
	 * of banks found contains all banks of this test run.
	 */
	public void testFindBanks()
	{
		final Collection<Bank> bs = finance.findBanks();
		assertNotNull(bs);
		assertTrue(bs.size() > 0);
		assertTrue(bs.containsAll(banks));
	}

//	/**
//	 * Block transfer funds from one randomly selected source account to multiple
//	 * target accounts. Test consists of two rounds: i) transfer small amounts that
//	 * do not overdraw source account and assert that balances are correct after
//	 * performing the bulk transfer. ii) transfer large amounts that will overdraw
//	 * the source account and assert that balances have not changed after performing
//	 * the block transfer.
//	 */
//	public void testBlockTransfer()
//	{
//		final int numberOfPayees = 8;
//		Customer payer = null;
//		Account payerAccount;
//		double payerBalance;
//		List<Customer> payees = new ArrayList<Customer>(numberOfPayees);
//
//		do // find an account that has sufficient balance for this test
//		{
//			while (payer == null) payer = extFinance.findCustomer(random.nextInt(NUMBER_OF_CUSTOMERS / 2));
//			payerAccount = payer.getAccounts().get(0);
//			payerBalance = payerAccount.getBalance();
//		} while (payerBalance < 1000.0);
//
//
//		for (long i = payer.getCustomerID() + 1; payees.size() < numberOfPayees; i++)
//		{
//			final Customer tmp = extFinance.findCustomer(i);
//			if (tmp == null) continue;
//			payees.add(tmp);
//		}
//		// OK, now we have almost randomly selected customers, all distinct (but we do
//		// not care whether their accounts are at distinct banks!)
//
//
//		// **********************************************************************
//		// First Round
//		// Test with small transfer amount that does not overdraw source account.
//		// (numberOfPayees * amount < payerBalance)
//		// **********************************************************************
//		long blockTransferID = extFinance.createBlockTransfer(payerAccount);
//		double amount = 2.0;
//		for (int i = 0; i < payees.size(); i++)
//		{
//			extFinance.addToBlockTransfer(payees.get(i).getAccounts().get(0), amount, blockTransferID);
//		}
//		try
//		{
//			extFinance.performBlockTransfer(blockTransferID);
//			final Customer payerClone = extFinance.findCustomer(payer.getCustomerID());
//			final Account payerCloneAccount = payerClone.getAccounts().get(0);
//			final double payerCloneBalance = payerCloneAccount.getBalance();
//			assertEquals(payer.getCustomerID(), payerClone.getCustomerID());
//			assertEquals(payerAccount.getAccountID(), payerCloneAccount.getAccountID());
//			assertEquals(payerBalance - (numberOfPayees * amount), payerCloneBalance);
//
//			for (int i = 0; i < payees.size(); i++)
//			{
//				final Customer payee = payees.get(i);
//				final Account payeeAccount = payee.getAccounts().get(0);
//				final double payeeBalance = payeeAccount.getBalance();
//				final Customer payeeClone = extFinance.findCustomer(payee.getCustomerID());
//				final Account payeeCloneAccount = payeeClone.getAccounts().get(0);
//				final double payeeCloneBalance = payeeCloneAccount.getBalance();
//				assertEquals(payee.getCustomerID(), payeeClone.getCustomerID());
//				assertEquals(payeeAccount.getAccountID(), payeeCloneAccount.getAccountID());
//				assertEquals(payeeBalance + amount, payeeCloneBalance);
//			}
//		}
//		catch (final Exception e)
//		{
//			fail("Unexpected exception " + e);
//		}
//
//
//		// ***********************************************************************
//		// Second Round
//		// Test again with large transfer amount that does overdraw source account
//		// (numberOfPayees * amount > payerBalance)
//		// ***********************************************************************
//
//		// get current state of all customers from server because each was changed during first round
//		payer = extFinance.findCustomer(payer.getCustomerID());
//		payerAccount = payer.getAccounts().get(0);
//		payerBalance = payerAccount.getBalance();
//		final List<Customer> tmp = new ArrayList<Customer>(payees.size());
//		for (Customer customer : payees)
//		{
//			customer = extFinance.findCustomer(customer.getCustomerID());
//			tmp.add(customer);
//		}
//		assertEquals(payees.size(), tmp.size()); // sanity check
//		payees = tmp;
//
//		blockTransferID = extFinance.createBlockTransfer(payerAccount);
//		amount = 10000.0;
//		for (int i = 0; i < payees.size(); i++)
//		{
//			extFinance.addToBlockTransfer(payees.get(i).getAccounts().get(0), amount, blockTransferID);
//		}
//		try
//		{
//			extFinance.performBlockTransfer(blockTransferID);
//			fail(AccountOverdrawException.class.getSimpleName() + " exception expected.");
//		}
//		catch (final Exception e)
//		{
//			assertTrue(e instanceof AccountOverdrawException);
//
//			final Customer payerClone = extFinance.findCustomer(payer.getCustomerID());
//			final Account payerCloneAccount = payerClone.getAccounts().get(0);
//			final double payerCloneBalance = payerCloneAccount.getBalance();
//			assertEquals(payer.getCustomerID(), payerClone.getCustomerID());
//			assertEquals(payerAccount.getAccountID(), payerCloneAccount.getAccountID());
//			assertEquals(payerBalance, payerCloneBalance);
//
//			for (int i = 0; i < payees.size(); i++)
//			{
//				final Customer payee = payees.get(i);
//				final Account payeeAccount = payee.getAccounts().get(0);
//				final double payeeBalance = payeeAccount.getBalance();
//				final Customer payeeClone = extFinance.findCustomer(payee.getCustomerID());
//				final Account payeeCloneAccount = payeeClone.getAccounts().get(0);
//				final double payeeCloneBalance = payeeCloneAccount.getBalance();
//				assertEquals(payee.getCustomerID(), payeeClone.getCustomerID());
//				assertEquals(payeeAccount.getAccountID(), payeeCloneAccount.getAccountID());
//				assertEquals(payeeBalance, payeeCloneBalance);
//			}
//		}
//	}
}
