/*
 * Created on 14.11.2006
 *
 * (c) 2006 Uni Basel Switzerland - Thorsten M�ller
 */
package ch.unibas.cs.dbis.dis.ejb3;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

/**
 * @author Thorsten M�ller - Thorsten.Moeller@unibas.ch
 * @version $Rev: 107 $; $Author: thorsten $; $Date: 2008-09-04 17:54:06 +0200 (Do, 04 Sep 2008) $
 * @uml.dependency   supplier="ch.unibas.cs.dbis.dis.ejb3.AccountOverdrawException" stereotypes="Standard::Uses"
*  
 */
@Entity
public class Account implements Serializable
{
	private static final long serialVersionUID = 7443656659402964861L;

	/**
	 * @uml.property  name="balance"
	 */
	private double balance;

	/**
	 * @uml.property  name="accountID"
	 */
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long accountID;

	/**
	 * @uml.property  name="customer"
	 * @uml.associationEnd  inverse="accounts:ch.unibas.cs.dbis.dis.ejb3.Customer"
	 */
	@ManyToOne(fetch=FetchType.EAGER)
	private Customer customer;
	@ManyToOne(fetch=FetchType.EAGER)
	private Bank bank;

	/**
	 * Needed by the JavaEE container.
	 */
	Account()
	{
		super();
	}

	/**
	 * @param owner The owner of the new account.
	 */
	public Account(final Customer owner)
	{
		this.customer = owner;
	}

	/**
	 * @return  the accountID
	 * @uml.property  name="accountID"
	 */
	public long getAccountID()
	{
		return accountID;
	}

	/**
	 * @param  newID
	 * @uml.property  name="accountID"
	 */
	void setAccountID(final long newID)
	{
		this.accountID = newID;
	}

	/**
	 * @return  the balance
	 * @uml.property  name="balance"
	 */
	public double getBalance()
	{
		return balance;
	}

	/**
	 * @param  newBalance
	 * @uml.property  name="balance"
	 */
	void setBalance(final double newBalance)
	{
		this.balance = newBalance;
	}

	/**
	 * @return the bank
	 */
	public Bank getBank()
	{
		return bank;
	}

	/**
	 * @param bank the bank to set
	 */
	public void setBank(final Bank bank)
	{
		this.bank = bank;
	}

	/**
	 * @return  the customerID
	 * @uml.property  name="customer"
	 */
	public Customer getCustomer()
	{
		return customer;
	}

	/**
	 * @param  newCustomer
	 * @uml.property  name="customer"
	 */
	void setCustomer(final Customer newCustomer)
	{
		this.customer = newCustomer;
	}

	/**
	 * @param amount Deposit the given amount to the account.
	 * @throws IllegalArgumentException if the given amount is less than zero.
	 */
	public void deposit(final double amount)
	{
		if (amount < 0) throw new IllegalArgumentException("Deposit of negative amount is not allowed");
		setBalance(getBalance() + amount);
	}

	/**
	 * @param amount Withdraw the given amount from the account.
	 * @throws IllegalArgumentException if the given amount is less than zero.
	 * @throws AccountOverdrawException in case given amount is greater than account balance.
	 */
	public void withdraw(final double amount) throws AccountOverdrawException
	{
		if (amount < 0) throw new IllegalArgumentException("Withdraw of negative amount is not allowed");
		if (amount > balance) throw new AccountOverdrawException("The operation would lead to overdrawing the account and was rejected.");
		setBalance(getBalance() - amount);
	}

	/* @see java.lang.Object#toString() */
	@Override
	public String toString()
	{
		return "Account #" + accountID + ", owned by customer ID " + customer.getCustomerID();
	}
}
