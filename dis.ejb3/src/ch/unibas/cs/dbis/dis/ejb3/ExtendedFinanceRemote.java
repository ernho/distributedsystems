/*
 * Created on 17.10.2008
 *
 * (c) 2008 Thorsten M�ller - University of Basel Switzerland
 */
package ch.unibas.cs.dbis.dis.ejb3;

/**
 *
 * @author Thorsten M�ller - Thorsten.Moeller@unibas.ch
 * @version $Rev: 110 $; $Author: thorsten $; $Date: 2008-10-17 20:42:35 +0200 (Fr, 17 Okt 2008) $
 */
public interface ExtendedFinanceRemote extends ExtendedFinance
{

}
