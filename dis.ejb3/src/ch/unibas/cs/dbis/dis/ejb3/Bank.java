/*
 * Created on 14.11.2006
 *
 * (c) 2006 Uni Basel Switzerland - Thorsten M�ller
 */
package ch.unibas.cs.dbis.dis.ejb3;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

import com.sun.istack.NotNull;

/**
 *
 * @author Thorsten M�ller - Thorsten.Moeller@unibas.ch
 * @version $Rev: 107 $; $Author: thorsten $; $Date: 2008-09-04 17:54:06 +0200 (Do, 04 Sep 2008) $
 */
@Entity
@NamedQuery(
		name = "findAllBanks",
		query = "select object(b) from Bank b"
)
public class Bank implements Serializable
{
	private static final long serialVersionUID = -5286009438468201802L;
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE) 
	private long bankID;
	//@NotNull //braucht man notnull?
	private String name;
	

	/**
	 * @uml.property name="accounts"
	 * @uml.associationEnd multiplicity="(0 -1)" ordering="true" aggregation="shared"
	 */
	@OneToMany(fetch=FetchType.LAZY, mappedBy="bank")
	private List<Account> accounts;

	/**
	 * @uml.property name="customers"
	 * @uml.associationEnd multiplicity="(0 -1)" ordering="true" aggregation="shared"
	 */
	@OneToMany(fetch=FetchType.LAZY, mappedBy="bank")
	private List<Customer> customers;

	/**
	 * Needed by the JavaEE container.
	 */
	Bank()
	{
		super();
	}

	public Bank(final String name)
	{
		this.name = name;
		this.accounts = new ArrayList<Account>();
		this.customers = new ArrayList<Customer>();
	}

	/**
	 * @return the bankID
	 */
	public long getBankID()
	{
		return bankID;
	}

	/**
	 * @param bankID the bankID to set
	 */
	void setBankID(final long bankID)
	{
		this.bankID = bankID;
	}

	/**
	 * @return the accounts
	 */
	public List<Account> getAccounts()
	{
		return accounts;
	}

	/**
	 * @param accounts
	 */
	void setAccounts(final List<Account> accounts)
	{
		this.accounts = accounts;
	}

	/**
	 * @return the customers
	 */
	public List<Customer> getCustomers()
	{
		return customers;
	}

	/**
	 * @param customers the customers to set
	 */
	void setCustomers(final List<Customer> customers)
	{
		this.customers = customers;
	}

	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(final String name)
	{
		this.name = name;
	}

	/**
	 * A bank is defined to be equal to another bank if both bank IDs are equal.
	 *
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj)
	{
		if((obj == null) || (!obj.getClass().equals(this.getClass()))) return false;
		return bankID == ((Bank) obj).bankID;
	}

	/* @see java.lang.Object#hashCode() */
	@Override
	public int hashCode()
	{
		return 71 + (int) (bankID ^ (bankID >>> 32));
	}
}
