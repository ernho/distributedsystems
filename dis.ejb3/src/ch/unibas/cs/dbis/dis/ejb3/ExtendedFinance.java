/*
 * Created on 17.10.2008
 *
 * (c) 2008 Thorsten M�ller - University of Basel Switzerland
 */
package ch.unibas.cs.dbis.dis.ejb3;

/**
 * This interface specifies methods that realize a <em>simplified</em> block
 * transfer. Obviously, it should not be used AS IS in real life scenarios,
 * for various reasons!
 *
 * @author Thorsten M�ller - Thorsten.Moeller@unibas.ch
 * @version $Rev: 110 $; $Author: thorsten $; $Date: 2008-10-17 20:42:35 +0200 (Fr, 17 Okt 2008) $
 */
public interface ExtendedFinance extends Finance
{
	/**
	 * Create a new block transfer for the given account.
	 *
	 * @param source The account that servers as the source for all transfers
	 * 	that will be added subsequently.
	 * @return A unique identifier that has been assigned to the block transfer.
	 * 	It needs to be used subsequently to add single transfers, cancel, or
	 * 	to perform it.
	 */
	public long createBlockTransfer(Account source);

	/**
	 * Add a single transfer to some already created block transfer.
	 *
	 * @param target The target account.
	 * @param amount The amount to transfer to the target account.
	 * @param blockTransferID The ID of the block transfer that was created before.
	 * @throws IllegalArgumentException If the given block transfer ID does not exist.
	 */
	public void addToBlockTransfer(Account target, double amount, long blockTransferID)
		throws IllegalArgumentException;

	/**
	 * Cancel/remove the given block transfer.
	 *
	 * @param blockTransferID The ID of the block transfer to cancel.
	 * @throws IllegalArgumentException If the given block transfer ID does not exist.
	 */
	public void cancelBlockTransfer(long blockTransferID) throws IllegalArgumentException;

	/**
	 * Performs all transfers that the specified block transfer consists of,
	 * that is, try to make them persistent.
	 * <p>
	 * If this method returns with an exception <em>none</em> of the single
	 * transfers will be made persistent, that is, the balance of the source and
	 * all target account(s) will not be changed as a result of this method
	 * invocation.
	 * <p>
	 * No matter how this method returns, the block transfer ID must be disposed
	 * by the system and must no longer be used.
	 *
	 * @param blockTransferID The ID of the block transfer to perform/persist.
	 * @throws AccountOverdrawException If the sum of all single transfers
	 * 	exceeds the current source account balance.
	 * @throws IllegalArgumentException If the given block transfer ID does not exist.
	 */
	public void performBlockTransfer(long blockTransferID)
		throws AccountOverdrawException, IllegalArgumentException;

}
