/*
 * Created on 14.11.2006
 * 
 * (c) 2006 Uni Basel Switzerland - Thorsten M�ller
 */
package ch.unibas.cs.dbis.dis.ejb3;

/**
 * Used to handle the event when one tried to overdraw a certain account, i.e.,
 * if {@link Account#withdraw(double)} was called with a value which is greater
 * than the current account balance.
 * 
 * @author Thorsten M�ller - Thorsten.Moeller@unibas.ch
 * @version $Rev: 103 $; $Author: thorsten $; $Date: 2007-10-27 13:15:05 +0200 (Sa, 27 Okt 2007) $
 */
public class AccountOverdrawException extends Exception
{
	private static final long serialVersionUID = 6989369944006108931L;

	/**
	 * @see Exception#Exception().
	 */
	public AccountOverdrawException()
	{
		super();
	}

	/**
	 * @see Exception#Exception(String, Throwable)
	 */
	public AccountOverdrawException(String message, Throwable cause)
	{
		super(message, cause);
	}

	/**
	 * @see Exception#Exception(String)
	 */
	public AccountOverdrawException(String message)
	{
		super(message);
	}

	/**
	 * @see Exception#Exception(Throwable)
	 */
	public AccountOverdrawException(Throwable cause)
	{
		super(cause);
	}
}
