/*
 * Created on 14.11.2006
 *
 * (c) 2006 Uni Basel Switzerland - Thorsten M�ller
 */
package ch.unibas.cs.dbis.dis.ejb3;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;


/**
 *
 * @author Thorsten M�ller - Thorsten.Moeller@unibas.ch
 * @version $Rev: 107 $; $Author: thorsten $; $Date: 2008-09-04 17:54:06 +0200 (Do, 04 Sep 2008) $
 */
@Entity
@NamedQuery(
		name = "queryCustomersByFullName",
		query = "SELECT Object (c) FROM Customer c WHERE c.firstname= ?1 AND c.surname=?2"
)
public class Customer implements Serializable
{
	private static final long serialVersionUID = 136067267969292098L;

	/**
	 * @uml.property  name="customerID"
	 */
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE) 
	private long customerID;

	/**
	 * @uml.property  name="firstname"
	 */
	private String firstname;

	/**
	 * @uml.property  name="surname"
	 */
	private String surname;

	/**
	 * @uml.property  name="accounts"
	 * @uml.associationEnd  multiplicity="(0 -1)" inverse="customer:ch.unibas.cs.dbis.dis.ejb3.Account"
	 */
	
	@OneToMany(fetch=FetchType.EAGER, mappedBy="customer", cascade=CascadeType.ALL)
	private List<Account> accounts;
	@ManyToOne(fetch=FetchType.EAGER)
	private Bank bank;

	/**
	 * Needed by the JavaEE container.
	 */
	Customer()
	{
		super();
	}

	/**
	 *
	 * @param firstname
	 * @param surname
	 */
	public Customer(final String firstname, final String surname)
	{
		this.firstname = firstname;
		this.surname = surname;
		accounts = new ArrayList<Account>();
	}

	/**
	 * @return the accounts
	 */
	public List<Account> getAccounts()
	{
		return accounts;
	}

	/**
	 * @param newAccounts
	 */
	void setAccounts(final List<Account> newAccounts)
	{
		this.accounts = newAccounts;
	}

	/**
	 * @return the bank
	 */
	public Bank getBank()
	{
		return bank;
	}

	/**
	 * @param bank the bank to set
	 */
	public void setBank(final Bank bank)
	{
		this.bank = bank;
	}

	/**
	 * @return  the customerID
	 * @uml.property  name="customerID"
	 */
	public long getCustomerID()
	{
		return customerID;
	}

	/**
	 * @param  newID
	 * @uml.property  name="customerID"
	 */
	void setCustomerID(final long newID)
	{
		this.customerID = newID;
	}

	/**
	 * @return  the firstname
	 * @uml.property  name="firstname"
	 */
	public String getFirstname()
	{
		return firstname;
	}

	/**
	 * @return  the surname
	 * @uml.property  name="surname"
	 */
	public String getSurname()
	{
		return surname;
	}

	/**
	 * @param firstname  the firstname to set
	 * @uml.property  name="firstname"
	 */
	public void setFirstname(final String firstname)
	{
		this.firstname = firstname;
	}

	/**
	 * @param surname  the surname to set
	 * @uml.property  name="surname"
	 */
	public void setSurname(final String surname)
	{
		this.surname = surname;
	}

	/**
	 * @param newAccount The new account to add.
	 * @return <code>true</code> if the new account was added, <code>false</code> otherwise.
	 */
	public boolean addAccount(final Account newAccount)
	{
		return accounts.add(newAccount);
	}

	/**
	 * @param account The account to remove.
	 * @return <code>true</code> if the account was removed, <code>false</code> otherwise.
	 */
	public boolean removeAccount(final Account account)
	{
		return accounts.remove(account);
	}

	/* @see java.lang.Object#toString() */
	@Override
	public String toString()
	{
		return "Customer #" + customerID + ", first name " + firstname + ", surname " + surname + ", accounts "
			+ accounts;
	}
}
