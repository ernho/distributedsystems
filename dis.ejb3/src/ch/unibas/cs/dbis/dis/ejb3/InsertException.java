/*
 * Created on 27.11.2006
 * 
 * (c) 2006 Uni Basel Switzerland - Thorsten M�ller
 */
package ch.unibas.cs.dbis.dis.ejb3;


/**
 * 
 * @author Thorsten M�ller - Thorsten.Moeller@unibas.ch
 * @version $Rev: 97 $; $Author: thorsten $; $Date: 2007-01-29 11:00:20 +0100 (Mo, 29 Jan 2007) $
 *
 */
public class InsertException extends Exception
{
	private static final long serialVersionUID = 2119638114715204839L;
	
	/**
	 * 
	 */
	public InsertException()
	{
		super();
	}
	
	/**
	 * @see Exception#Exception(String, Throwable)
	 */
	public InsertException(String message, Throwable cause)
	{
		super(message, cause);
	}

	/**
	 * @see Exception#Exception(String)
	 */
	public InsertException(String message)
	{
		super(message);
	}

	/**
	 * @see Exception#Exception(Throwable)
	 */
	public InsertException(Throwable cause)
	{
		super(cause);
	}
}
